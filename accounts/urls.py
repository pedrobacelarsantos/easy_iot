from django.urls import path
from . import views

from .views import FuncionarioSignUpView, AdministradorSignUpView, escolha_de_signup

urlpatterns = [
    path('signup/', escolha_de_signup, name="signup"),
    path('signup/funcionario/', FuncionarioSignUpView.as_view(), name='funcionario_signup'),
    path('signup/administrador/', AdministradorSignUpView.as_view(), name='administrador_signup'),
]
