from django.db.models import fields
from rest_framework import serializers
from APP_iot import models
# from accounts import models

class Device_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Device
        fields = '__all__'

class Type_Device_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type_Device
        fields = '__all__'


class Event_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Event
        fields = '__all__'
