from rest_framework import viewsets
from . import serializers
from APP_iot import models


class Device_ViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.Device_Serializer
    queryset = models.Device.objects.all()


class Type_Device_ViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.Type_Device_Serializer
    queryset = models.Type_Device.objects.all()

class Event_ViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.Event_Serializer
    queryset = models.Event.objects.all()



